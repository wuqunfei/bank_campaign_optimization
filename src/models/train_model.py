import mlflow
import pandas as pd
import numpy
import pathlib
import pprint

from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score, roc_auc_score, confusion_matrix, classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from loguru import logger
from src.features.build_features import create_preprocessor, get_data_set, split_data_set, get_features_group


def get_classifiers() -> list:
    return [
        LogisticRegression(),
        RandomForestClassifier(),
        KNeighborsClassifier(),
        LinearSVC(),
        DecisionTreeClassifier(),
        AdaBoostClassifier(),
        GradientBoostingClassifier(),
        MLPClassifier()
    ]


def get_hyper_parameter_grid(classifier_name: str, class_weight: dict) -> dict:
    hyper_parameter_grids = {
        'LogisticRegression': {
            'classifier__class_weight': ['balanced', class_weight, None]
        },
        'RandomForestClassifier': {
            'classifier__n_estimators': [400, 600],
            'classifier__class_weight': ['balanced', class_weight]
        },
        'KNeighborsClassifier': {
            'classifier__n_neighbors': [4, 8]
        },
        'LinearSVC': {
            'classifier__class_weight': ['balanced', class_weight]
        },
        'DecisionTreeClassifier': {
            'classifier__class_weight': ['balanced', class_weight]
        },
        'GradientBoostingClassifier': {
            'classifier__learning_rate': [0.05, 0.1],
            'classifier__n_estimators': [100, 500]
        },
        'MLPClassifier': {
            'classifier__random_state': [1, 2],
            'classifier__max_iter': [200],
            'classifier__hidden_layer_sizes': [(100,)]
        }
    }
    return hyper_parameter_grids.get(classifier_name, {})


def get_profit(ture_positive_n: int, false_positive_n: int) -> int:
    new_contact_profit = 80
    call_cost = 8
    profit = (ture_positive_n * new_contact_profit - ture_positive_n * call_cost) - false_positive_n * call_cost
    return profit


def tracing_to_mlflow(classifier_name: str, gsc: GridSearchCV, metrics: dict, binary_features_to_numerical: bool) -> None:
    # project_dir = pathlib.Path(__file__).resolve().parents[2]
    # project_model_dir = project_dir.joinpath('models')
    # uri = f'file:{project_model_dir}'
    # mlflow.set_tracking_uri(uri=uri)
    model_artifact_path = 'models'
    # Save model file into storage by mlflow
    mlflow.sklearn.log_model(sk_model=gsc.best_estimator_, artifact_path=model_artifact_path)
    # Save parameters of the model
    mlflow.log_param('k-folds', gsc.cv)
    mlflow.log_param('classifier', classifier_name)
    mlflow.log_param('binary-features-N', binary_features_to_numerical)
    mlflow.log_params(gsc.best_params_)
    # Save metrics of the model
    mlflow.log_metric('accuracy', gsc.best_score_)
    mlflow.log_metrics(metrics)
    logger.info(f'{classifier_name} - best score {gsc.best_score_} with parameters \r\n{pprint.pformat(gsc.best_params_)}')


def get_metrics(classifier_name: str, y_test: pd.Series, y_pred: numpy.ndarray) -> dict:
    f1 = f1_score(y_test, y_pred, average='macro')
    auc = roc_auc_score(y_test, y_pred)
    matrix = confusion_matrix(y_test, y_pred)
    tp = matrix[1][1]
    fp = matrix[0][1]
    profit_plan = get_profit(tp, fp)
    profit_origin = get_profit(y_test[y_test == 1].size, y_test[y_test == 0].size)
    call_plan = tp + fp
    call_origin = y_test.size
    metrics = {
        'f1': f1,
        'auc': auc,
        'profit_plan': profit_plan,
        'profit_origin': profit_origin,
        'call_plan': call_plan,
        'call_origin': call_origin,
        'call_reduce_rate': (call_origin - call_plan) / call_origin
    }
    logger.info(f'{classifier_name} - report \r\n{classification_report(y_test, y_pred)}')
    logger.info(f'{classifier_name} - confusion_matrix \r\n{matrix}')
    logger.info(f'{classifier_name} - metrics \r\n{pprint.pformat(metrics)}')
    return metrics


def get_class_weight(df: pd.DataFrame) -> dict:
    volume = df['y'].value_counts(normalize=True)
    class_weight_reverse = {0: volume[1], 1: volume[0]}
    return class_weight_reverse


def choose_preprocessor(binary_features_to_numerical: bool, df: pd.DataFrame) -> ColumnTransformer:
    features = get_features_group()
    preprocessor = None
    if binary_features_to_numerical:
        preprocessor = create_preprocessor(features['numerical_features_with_binary'], features['categorical_features'])
    else:
        preprocessor = create_preprocessor(features['numerical_features'], features['categorical_features_with_binary'])
        for feature_name in features['binary_features']:
            df[feature_name] = df[feature_name].astype(str)
    return preprocessor, df


def core_train(input_file_path: str, binary_features_to_numerical: bool) -> None:
    # Prepare parameters
    classifiers = get_classifiers()

    df = get_data_set(input_file_path)
    preprocessor, df = choose_preprocessor(binary_features_to_numerical, df)
    x_train, x_test, y_train, y_test = split_data_set(df)
    class_weight = get_class_weight(df)

    mlflow.set_experiment(experiment_name='bank_campaign')
    experiment_id = mlflow.get_experiment_by_name('bank_campaign').experiment_id

    for classifier in classifiers:
        classifier_name = classifier.__class__.__name__
        grid = get_hyper_parameter_grid(classifier_name, class_weight=class_weight)
        with mlflow.start_run(run_name=classifier_name, experiment_id=experiment_id) as run:
            try:
                logger.success(f"Classifier: {classifier_name}, is training...")
                pipe = Pipeline(steps=[('preprocessor', preprocessor), ('classifier', classifier)])
                gsc = GridSearchCV(pipe, param_grid=grid, n_jobs=1, scoring='roc_auc', cv=2)
                gsc.fit(x_train, y_train)
                y_pred = gsc.predict(x_test)
                metrics = get_metrics(classifier_name=classifier_name, y_test=y_test, y_pred=y_pred)
                tracing_to_mlflow(classifier_name, gsc, metrics, binary_features_to_numerical)
                logger.info(f"{classifier_name} run id: # {run.info.run_id}")
            except Exception as ex:
                logger.error(f"Classifier: {classifier_name}, train error:")
                logger.exception(ex)
