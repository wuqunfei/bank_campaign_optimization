import streamlit as st

from sklearn.ensemble import GradientBoostingClassifier
from loguru import logger
import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline

from src.features.build_features import get_data_set, split_data_set, get_features_group, create_preprocessor
from src.models.train_model import get_metrics

st.title('Bank Campaign Report')


def top_classifier_features():
    input_file_path = '/Users/qunfei/gda/bank_campaign_optimization/data/processed/bank-processed.csv'
    features = get_features_group()
    preprocessor = create_preprocessor(features['numerical_features_with_binary'], features['categorical_features'])
    df = get_data_set(input_file_path)
    x_train, x_test, y_train, y_test = split_data_set(df)
    gbc = GradientBoostingClassifier()
    logger.success(f"Classifier: {gbc}, is training...")
    pipe = Pipeline(steps=[('preprocessor', preprocessor), ('gbc', gbc)])
    pipe.fit(x_train, y_train)
    y_pred = pipe.predict(x_test)
    get_metrics(classifier_name=gbc.__class__.__name__, y_test=y_test, y_pred=y_pred)

    numerical_features = pipe.named_steps['preprocessor'].transformers_[0][2]
    categorical_features = pipe.named_steps['preprocessor'].transformers_[1][1]['onehot'].get_feature_names(features['categorical_features'])
    features_list = numerical_features + categorical_features

    importance_list = gbc.feature_importances_
    logger.info(importance_list)
    # TODO don't use pipeline directly, because the feature index is not match the original feature list.
    # Feature ranking:
    # 1.
    # duration: 0.282091
    # 2.
    # euribor3m: 0.094536
    # 3.
    # age: 0.079953
    # 4.
    # nr.employed: 0.049350
    # 5.
    # campaign: 0.039866
    # 6.
    # pdays: 0.030400

    # TODO pick one feature with campaign and y, find some insight and rule of key feature.

# https://github.com/onehungrybird/Bank-Marketing-DataSet-Analysis/blob/master/Bank_Marketing_DataSet_Analysis.ipynb

top_classifier_features()
