# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder


def get_features_group():
    binary_features = ['default', 'housing', 'loan', 'contact']
    numerical_features = ['nr.employed', 'previous', 'campaign', 'cons.conf.idx', 'duration', 'cons.price.idx', 'emp.var.rate', 'euribor3m', 'age', 'pdays']
    categorical_features = ['job', 'marital', 'education', 'month', 'day_of_week', 'poutcome']

    return {
        'binary_features': binary_features,

        'numerical_features': numerical_features,
        'numerical_features_with_binary': numerical_features + binary_features,

        'categorical_features': categorical_features,
        'categorical_features_with_binary': categorical_features + binary_features
    }


def missing_values(df, categorical_features):
    for feature in categorical_features:
        df[feature].replace('unknown', np.nan, inplace=True)
    return df


def binary_encode(df):
    binary_features = ['default', 'housing', 'loan', 'contact', 'y']
    for f in binary_features:
        df.loc[df[f] == 'no', f] = 0
        df.loc[df[f] == 'yes', f] = 1
        if f == 'contact':
            df.loc[df[f] == 'telephone', f] = 0
            df.loc[df[f] == 'cellular', f] = 1
    return df


def filter_source(df: pd.DataFrame, output_filepath: str):
    features_group = get_features_group()
    df = missing_values(df, features_group['categorical_features_with_binary'])
    df = binary_encode(df)
    df.to_csv(path_or_buf=output_filepath, index=False)


def get_data_set(input_file_path: str):
    dataset = pd.read_csv(input_file_path)
    return dataset


def split_data_set(dataset: pd.DataFrame):
    x, y = dataset.drop('y', axis=1), dataset['y']
    return train_test_split(x, y, test_size=0.2, random_state=40)


def create_preprocessor(numerical_features: list, categorical_features: list):
    numeric_transformer = Pipeline(steps=[('imputer', SimpleImputer(strategy='median')), ('scaler', StandardScaler())])
    categorical_transformer = Pipeline(steps=[('imputer', SimpleImputer(strategy='constant', fill_value='missing')), ('onehot', OneHotEncoder(handle_unknown='ignore'))])
    preprocessor = ColumnTransformer(transformers=[
        ('numeric_transformer', numeric_transformer, numerical_features),
        ('categorical_transformer', categorical_transformer, categorical_features)])

    return preprocessor
