# -*- coding: utf-8 -*-
import click
from loguru import logger
import pandas as pd
from pandas.core.frame import DataFrame
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

from src.features.build_features import filter_source
from src.models.train_model import core_train


def load_data(file_path: str) -> DataFrame:
    df = pd.read_csv(filepath_or_buffer=file_path, delimiter=";")
    return df


@click.command()
@click.option('--input_filepath', type=click.Path(exists=True))
@click.option('--output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info('making final data set from raw data')
    df = load_data(input_filepath)
    filter_source(df, output_filepath)
    logger.info("train model from processed file")
    core_train(input_file_path=output_filepath, binary_features_to_numerical=True)


if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
