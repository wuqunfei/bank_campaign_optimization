# Business Explain
Precondition, If next week, we need to call 20% of random customer form list, number of customer is 8038. we can use machine learning model to predict who need to call or not. 
Suggestions as following, 
- Business can reduce the call deeply.
- If the target group 100% need to call, 77% call can be reduced.
- Profit can be higher in less call.
- Call person who deposit, we can get $72, True Positive Case(TP)
- Call person who does not deposit, we lost $7, False Positive Case(FP)

# Technological Explain
- AUC Score is the key metrics, AUC and Profit has linearity by Business assumption and algorithm proof.     
- Profits = (TruePositive * 80 - TruePositive * 8 ) - (FalsePositive * 8 )， True/False = Whether Customer Deposit, Positive = Call Customer
- The best Model has AUC score 0.867, call 1896 customer in plan, reduce 1896/8038 = 77% call, get $54032 profits 
- In microcosm, we can use predict api to decide whether call the customer by his/her feature
- In macrocosm, some models can get the most important features, we can reverse engineering to get the simples rules for human. But it is impossible for some models like (Neural network model). 

# Library and Framework tools
- Using Different Classifier and Different Hyper meters with  `[GridSearchCV](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html)`
- Using Mlflow to save the metrics and model 
- Using Bentoml(https://docs.bentoml.org/en/latest/)to deploy model without any Devops works easily.
- Using notebook only explore the data the model, not for production.
- Using function programming to decoupling the multi-steps in python.


# Improvement not done yet
- Missing numerical data can use KNN Imputation or Advanced Imputation
- Missing Categorical data can randomForestClassifier to full 
- Some data has strong outliers like campaigns, age, before StrandScalar to deal
- Code can be writ in OOP with Unit Test. currently, no time to do it. 
- Try to use smote library to balance the oversampling Y data, currently class_weigh = balance may is not perfect.
- Some algorithm hypermeter tunning does not work by limited hardware resources.
- how to solve the FN case for Potential customer need to consider.  






